/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package xyz.nekoit.mavenproject1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Archivist
 */
public class Mavenproject1Test {
    
    public Mavenproject1Test() {
    }

    /**
     * Test of main method, of class Mavenproject1.
     */
    @Test
    public void testMain() {
        String[] args = new String[] {  };
        Mavenproject1.main(args);
        assertEquals(0, args.length);
    }
    
}
